#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 11:12 2021
@author: genevieverobin
"""
# %load_ext autoreload
# %autoreload 2
import math
import numpy as np
import matplotlib.pyplot as plt
import algos_sampling
from numpy import genfromtxt
from importlib import reload
reload(algos_sampling)

def V(param):
    return math.sin(2 * math.pi * param) * (2 + math.sin(math.pi * param))

def gradV(param):
    grad_val = 2 * math.pi * math.cos(2 * math.pi * param) * (2 + math.sin(math.pi * param))
    grad_val = grad_val + math.pi * math.cos(math.pi * param) * math.sin(2 * math.pi * param)
    return grad_val

constr = 'l2'
xmin = -1
xmax = 1
beta = 1
# Parameters
#filename="../../recherche/post-doc/diffusion-code/results/D_optim.txt"
filename="results/D_optim.txt"
D_optim = genfromtxt(filename, delimiter=' ', dtype='float32')
plt.plot(D_optim)

I = D_optim.shape[0]
mesh = np.linspace(xmin, xmax, I + 1)
dx = (xmax - xmin) / I

pi = np.zeros(I)
for i in range(I):
    pi[i] = math.exp(-beta * V((mesh[i] + mesh[i + 1]) / 2))
pi = pi / (sum(pi) * dx)
plt.plot(mesh[range(I)], pi)




D_homog = np.zeros(I)
for i in range(I):
    D_homog[i] = math.exp((2 * beta * V(mesh[i]) / 3))
D_homog = D_homog /  math.sqrt(np.dot(D_homog, pi * D_homog) * dx)

"""
Simulations MALA

q = -0.75
dt = 2*1e-3 # 1 % rejection
# dt = 0.01 # 20% rejections
N = 1000000
nb_rejections = 0
qtrace = np.zeros((N, 1))
for n in range(N):
    q, rejection_flag = algos_sampling.step_mala(q, dt, V, gradV, beta, xmin, xmax)
    if rejection_flag:  # there was a rejection in MALA
        nb_rejections += 1
    qtrace[n] = q

print("Average rejection rate", nb_rejections * 1. / N)

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
axes[0].plot(qtrace)
axes[1].hist(qtrace, bins=100, density=True)
fig.tight_layout()
plt.hist(qtrace, bins=100, density=True)
plt.plot(mesh[range(I)], pi)

q0 = -0.75
q = q0
dt = 1e-5 # 1% rejections
#dt = 1e-3 # 20 % rejection
N = 1000000

nb_rejections = 0
qtrace_optim = np.zeros((N, 1))
for n in range(N):
    q, rejection_flag = algos_sampling.step_mala_optim(q, dt, D_optim, dx, V, gradV, beta, xmin, xmax)
    if rejection_flag:  # there was a rejection in MALA
        nb_rejections += 1
    qtrace_optim[n] = q

print("Average rejection rate", nb_rejections * 1. / N)

q = np.linspace(-1, 1, 1000)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
axes[0].plot(qtrace_optim)
axes[1].hist(qtrace_optim, bins=100, density=True)
fig.tight_layout()
plt.hist(qtrace_optim, bins=100, density=True)
plt.plot(mesh[range(I)], pi)

#D_homog = np.ones(I)
q0 = -0.75
q = q0
dt = 1e-4 # 1% rejection
# dt = 1e-2 # 20% rejection
N = 10000000
nb_rejections = 0
qtrace_homog = np.zeros((N, 1))
for n in range(N):
    q, rejection_flag = algos_sampling.step_mala_optim(q, dt, D_homog, dx, V, gradV, beta, xmin, xmax)
    if rejection_flag:  # there was a rejection in MALA
        nb_rejections += 1
    qtrace_homog[n] = q

print("Average rejection rate", nb_rejections * 1. / N)

q = np.linspace(-1, 1, 1000)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
axes[0].plot(qtrace_homog)
axes[1].hist(qtrace_homog, bins=100, density=True)
fig.tight_layout()
plt.hist(qtrace_homog, bins=100, density=True)
plt.plot(mesh[range(I)], pi)

fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(12, 6))
axes[0].hist(qtrace, bins=100, density=True)
axes[0].plot(mesh[range(I)] + dx / 2, pi)
axes[1].hist(qtrace_optim, bins=100, density=True)
axes[1].plot(mesh[range(I)] + dx / 2, pi)
axes[2].hist(qtrace_homog, bins=100, density=True)
axes[2].plot(mesh[range(I)] + dx / 2, pi)

np.savetxt("results/trajectory_homog_MALA.txt", qtrace_homog)
np.savetxt("results/trajectory_optim_MALA.txt", qtrace_optim)
np.savetxt("results/trajectory_MALA.txt", qtrace)
"""

"""
RWMH Simulations
"""
q0 = -0.75
q = q0
#dt = 2*1e-5 # 1% rejection
# dt = 2*1e-3 # 20% rejection
dt = 1e-3
N = 1000000
nb_rejections = 0
qtrace_RW = np.zeros((N, 1))
for n in range(N):
    q, rejection_flag = algos_sampling.step_RWMH(q, dt, V, beta, xmin, xmax, periodic = False)
    if rejection_flag:  # there was a rejection in MALA
        nb_rejections += 1
    qtrace_RW[n] = q

print("Average rejection rate", nb_rejections * 1. / N)

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
axes[0].plot(qtrace_RW[range(100000)])
axes[1].hist(qtrace_RW, bins=100, density=True)
fig.tight_layout()
plt.hist(qtrace_RW, bins=100, density=True)
plt.plot(mesh[range(I)], pi)

plt.plot(qtrace_RW[range(10000)])

q0 = -0.75
q = q0
#dt = 5*1e-6 # 1%rejection
# dt = 2*1e-3 # 20% rejection
dt = 1e-3
N = 1000000
nb_rejections = 0
qtrace_optim_RW = np.zeros((N, 1))
for n in range(N):
    q, rejection_flag = algos_sampling.step_RWMH_optim(q, dt, D_optim, dx, V, beta, xmin, xmax, periodic = False)
    if rejection_flag:  # there was a rejection in RWMH
        nb_rejections += 1
    qtrace_optim_RW[n] = q

print("Average rejection rate", nb_rejections * 1. / N)
plt.plot(qtrace_optim_RW[range(10000)])

q = np.linspace(-1, 1, 1000)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
axes[0].plot(qtrace_optim_RW)
axes[1].hist(qtrace_optim_RW, bins=100, density=True)
fig.tight_layout()
plt.hist(qtrace_optim_RW, bins=100, density=True)
plt.plot(mesh[range(I)], pi)

q0 = -0.75
q = q0
#dt = 1e-5 # 1% rejection
# dt = 5*1e-3 # 20 % rejection
dt = 1e-3
N = 1000000
nb_rejections = 0
qtrace_homog_RW = np.zeros((N, 1))
for n in range(N):
    q, rejection_flag = algos_sampling.step_RWMH_optim(q, dt, D_homog, dx, V, beta, xmin, xmax, periodic = False)
    if rejection_flag:  # there was a rejection in RWMH
        nb_rejections += 1
    qtrace_homog_RW[n] = q

print("Average rejection rate", nb_rejections * 1. / N)
plt.plot(qtrace_homog_RW[range(10000)])

q = np.linspace(-1, 1, 1000)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
axes[0].plot(qtrace_homog_RW)
axes[1].hist(qtrace_homog_RW, bins=100, density=True)
fig.tight_layout()
plt.hist(qtrace_homog_RW, bins=100, density=True)
plt.plot(mesh[range(I)], pi)

np.savetxt("results/trajectory_homog_RW_aperiodic.txt", qtrace_homog_RW)
np.savetxt("results/trajectory_optim_RW_aperiodic.txt", qtrace_optim_RW)
np.savetxt("results/trajectory_RW_aperiodic.txt", qtrace_RW)
