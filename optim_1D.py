#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 15:51:49 2020

@author: genevieverobin
"""

import numpy as np
import math
from scipy.sparse.linalg import eigsh
from scipy.sparse import csr_matrix
# from scipy.sparse import csc_matrix
import scipy.optimize as opt
from scipy.optimize import NonlinearConstraint
from scipy.optimize import Bounds

"""
// Optimisation (Projected gradient method) //

"""



def optim_algo(pi, dx, I, k=3, D_init=None, bound=1, tolerance=None, maxiter = 1000, disp = True, method = 'SLSQP'):

    M = np.zeros((I, I))
    for i in range(I - 1):
        M[i, i] = M[i, i] + pi[i] * dx / 3
        M[i + 1, i + 1] = M[i + 1, i + 1] + pi[i] * dx / 3
        M[i, i + 1] = M[i, i + 1] + pi[i] * dx / 6
        M[i + 1, i] = M[i + 1, i] + pi[i] * dx / 6
    M[I - 1, I - 1] = M[I - 1, I - 1] + pi[I - 1] * dx / 3
    M[0, 0] = M[0, 0] + pi[I - 1] * dx / 3
    M[I - 1, 0] = M[I - 1, 0] + pi[I - 1] * dx / 3
    M[0, I - 1] = M[0, I - 1] + pi[I - 1] * dx / 3
    M = csr_matrix(M)

    if D_init is None:
        D_init = 1. / pi
        D_init = D_init ** (2 / 3)
        D_init = D_init / math.sqrt(np.dot(D_init, pi * D_init) * dx)

    bounds = Bounds(0, np.inf)
    ineq_cons_norm = {'type': 'ineq',
                      'fun': lambda x: bound - np.dot(x, pi * x) * dx,
                      'jac': lambda x: - 2 * pi * x * dx}

    options = {'maxiter' : maxiter, 'disp' : disp}

    def f0(x):
        f_val, _ = lambda_value(x, pi, dx, I, M, k)
        return - f_val

    def Df0(x):
        return - lambda_grad(x, M, I, dx, pi, k)


    def callbackF(diff_iter):
        constr_term = - np.dot(Df0(diff_iter), pi*diff_iter)/(np.linalg.norm(Df0(diff_iter))*np.linalg.norm(pi*diff_iter))

        I2 = diff_iter.shape[0]
        A = np.zeros((I2, I2))
        for i in range(I2 - 1):
            A[i, i] = A[i, i] + diff_iter[i] * pi[i] * (1 / dx)
            A[i + 1, i + 1] = A[i + 1, i + 1] + diff_iter[i] * pi[i] * (1 / dx)
            A[i, i + 1] = A[i, i + 1] - diff_iter[i] * pi[i] * (1 / dx)
            A[i + 1, i] = A[i + 1, i] - diff_iter[i] * pi[i] * (1 / dx)
        A[I2 - 1, I2 - 1] = A[I2 - 1, I2 - 1] + diff_iter[I2 - 1] * pi[I2 - 1] * (1 / dx)
        A[0, 0] = A[0, 0] + diff_iter[I2 - 1] * pi[I2 - 1] * (1 / dx)
        A[I2 - 1, 0] = A[I2 - 1, 0] - diff_iter[I2 - 1] * pi[I2 - 1] * (1 / dx)
        A[0, I2 - 1] = A[0, I2 - 1] - diff_iter[I2 - 1] * pi[I2 - 1] * (1 / dx)
        A = csr_matrix(A)

        valp, vecp = eigsh(M, k=k, M=A + M)

        idx = np.argsort(valp)
        valp = valp[idx]
        vecp = np.real(vecp[:, idx[k - 2]])
        vecp = vecp / math.sqrt(np.dot(vecp, M.dot(vecp)))
        vp = np.zeros(I + 1)
        vp[range(I)] = vecp
        vp[I] = vp[0]
        lambda2 = (1 - np.real(valp[k - 2])) / np.real(valp[k - 2])
        lambda3 = (1 - np.real(valp[k - 3])) / np.real(valp[k - 3])

        print('{0: 3.12f}   {1: 3.12f}   {2: 3.12f}  {3: 3.12f}  {4: 3.12f}'.format(f0(diff_iter), constr_term, lambda3-lambda2, lambda2, lambda3))

    print('{0:9s}   {1:9s}   {2:9s}  {3:9s}  {4:9s}'.format(' f(X)', '<Df(X),pi>', 'lambda3-lambda2', 'lambda2', 'lambda3'))

    if method=='SLSQP':
        res = opt.minimize(f0, D_init, method='SLSQP', jac=Df0, constraints=ineq_cons_norm, tol=tolerance,
                           bounds = bounds, options = options, callback=callbackF)
    elif method == 'COBYLA':
        res = opt.minimize(f0, D_init, method='COBYLA', constraints=ineq_cons_norm, bounds=bounds,
                           options=options, callback=callbackF)
    elif method=='trust-constr':
        def cons_f(x):
            c = math.sqrt(np.dot(x, pi * x) * dx)
            return c

        def cons_J(x):
            return - 2 * pi * x * dx

        nonlinear_constraint = NonlinearConstraint(cons_f, 0, bound, jac=cons_J)
        res = opt.minimize(f0, D_init, method='trust-constr', jac=Df0, constraints=[nonlinear_constraint], bounds = bounds,
                           options = options)
    else:
        print("Unvalid optimization method, SLSQP used by default")
        res = opt.minimize(f0, D_init, method='SLSQP', jac=Df0, constraints=ineq_cons_norm, tol=tolerance,
                           bounds=bounds, options=options, callback=callbackF)

    return res


def lambda_value(D, pi, dx, I, M, k=3):
    # on fait la somme sur les éléments [x_i,x_{i+1}]
    I = int(I)
    A = np.zeros((I, I))
    for i in range(I - 1):
        A[i, i] = A[i, i] + D[i] * pi[i] * (1 / dx)
        A[i + 1, i + 1] = A[i + 1, i + 1] + D[i] * pi[i] * (1 / dx)
        A[i, i + 1] = A[i, i + 1] - D[i] * pi[i] * (1 / dx)
        A[i + 1, i] = A[i + 1, i] - D[i] * pi[i] * (1 / dx)
    A[I - 1, I - 1] = A[I - 1, I - 1] + D[I - 1] * pi[I - 1] * (1 / dx)
    A[0, 0] = A[0, 0] + D[I - 1] * pi[I - 1] * (1 / dx)
    A[I - 1, 0] = A[I - 1, 0] - D[I - 1] * pi[I - 1] * (1 / dx)
    A[0, I - 1] = A[0, I - 1] - D[I - 1] * pi[I - 1] * (1 / dx)
    A = csr_matrix(A)

    valp, vecp = eigsh(M, k=k, M=A + M)

    idx = np.argsort(valp)
    valp = valp[idx]
    vecp = np.real(vecp[:, idx[k - 2]])
    vecp = vecp / math.sqrt(np.dot(vecp, M.dot(vecp)))
    vp = np.zeros(I + 1)
    vp[range(I)] = vecp
    vp[I] = vp[0]
    lambdaD = (1 - np.real(valp[k - 2])) / np.real(valp[k - 2])
    return lambdaD, vp


def lambda_grad(D, M, I, dx, pi, k=3):
    I = int(I)
    A = np.zeros((I, I))
    for i in range(I - 1):
        A[i, i] = A[i, i] + D[i] * pi[i] * (1 / dx)
        A[i + 1, i + 1] = A[i + 1, i + 1] + D[i] * pi[i] * (1 / dx)
        A[i, i + 1] = A[i, i + 1] - D[i] * pi[i] * (1 / dx)
        A[i + 1, i] = A[i + 1, i] - D[i] * pi[i] * (1 / dx)
    A[I - 1, I - 1] = A[I - 1, I - 1] + D[I - 1] * pi[I - 1] * (1 / dx)
    A[0, 0] = A[0, 0] + D[I - 1] * pi[I - 1] * (1 / dx)
    A[I - 1, 0] = A[I - 1, 0] - D[I - 1] * pi[I - 1] * (1 / dx)
    A[0, I - 1] = A[0, I - 1] - D[I - 1] * pi[I - 1] * (1 / dx)
    A = csr_matrix(A)

    valp, vecp = eigsh(M, k=k, M=A + M)

    idx = np.argsort(valp)
    vecp = np.real(vecp[:, idx[k - 2]])
    vecp = vecp / math.sqrt(np.dot(vecp, M.dot(vecp)))
    vp = np.zeros(I + 1)
    vp[range(I)] = vecp
    vp[I] = vp[0]
    gradD = np.zeros(I)
    mat = np.array(([1, -1], [-1, 1]))
    for i in range(I):
        gradD[i] = np.dot(np.dot(mat, vp[range(i, i + 2)]), vp[i:(i + 2)]) * (pi[i] / dx)
    return gradD
