#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 14:19:00 2020

@author: genevieverobin
"""

import numpy as np
import math

"""
Potential
"""
def chi(g,d,x):
    y=0
    if x<g:
        y=1
    if (x<g and x<d):
        y=(math.pi/2+ np.arctan((1/(x-g) - 1/(d-x) )/2) )/math.pi
    return y

def gaussian_potential(x, y, Sigma):
    """Gaussian potential
    
    Parameters
    ----------
    x : {float, np.ndarray}
        X coordinate. Can be either a single number or an array. If you supply
        an array, x and y need to be the same shape.
    y : {float, np.ndarray}
        Y coordinate. Can be either a single number or an array. If you supply
        an array, x and y need to be the same shape.
    Sigma : {float, np.ndarray}
        Covariance matrix. Default is the identity matrix.
    Returns
    -------
    potential : {float, np.ndarray}
        Potential energy.
    """
    K = np.linalg.inv(Sigma)
    zz = np.dot([x,y],np.dot(K,[x,y]))/2
    #z= chi(3/4,9/10,zz)*(zz-0.9)
    return zz

#double well
def double_well_potential(x):
    """Double well
    
    Parameters
    ----------
    x : {float, np.ndarray}
        X coordinate. Can be either a single number or an array. If you supply
        an array, x and y need to be the same shape.
    y : {float, np.ndarray}
        Y coordinate. Can be either a single number or an array. If you supply
        an array, x and y need to be the same shape.
    Returns
    -------
    potential : {float, np.ndarray}
        Potential energy.
    """
    v = math.sin(2*math.pi*x)*(2+math.sin(math.pi*x))
    return v

def mueller_potential(x, y):
    """Mueller potential
    
    Parameters
    ----------
    x : {float, np.ndarray}
        X coordinate. Can be either a single number or an array. If you supply
        an array, x and y need to be the same shape.
    y : {float, np.ndarray}
        Y coordinate. Can be either a single number or an array. If you supply
        an array, x and y need to be the same shape.
    Returns
    -------
    potential : {float, np.ndarray}
        Potential energy. Will be the same shape as the inputs, x and y.
    
    Reference
    ---------
    Code adapted from https://cims.nyu.edu/~eve2/ztsMueller.m
    """
    
    aa = [-1, -1, -6.5, 0.7]
    bb = [0, 0, 11, 0.6]
    cc = [-10, -10, -6.5, 0.7]
    AA = [-200, -100, -170, 15]
    XX = [1, 0, -0.5, -1]
    YY = [0, 0.5, 1.5, 1]

    value = 0
    for j in range(0, 4):
        value += AA[j] * np.exp(aa[j] * (x - XX[j])**2 + bb[j] * (x - XX[j]) * (y - YY[j]) + cc[j] * (y - YY[j])**2)
    return value