#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 15:51:15 2020

@author: genevieverobin
"""
import math
import numpy as np

"""
MALA algorithm classical
"""

def step_mala(param, time_step, potential, grad_potential, beta, xmin, xmax, periodic = True):
  reject=False
  G=np.random.normal(size=1)
  param_tilde = param-time_step*grad_potential(param)+math.sqrt(2*time_step/beta)*G
  log_r = beta*(potential(param)-potential(param_tilde))
  Gtilde2 = (param-param_tilde+time_step*grad_potential(param_tilde))**2*beta/(2*time_step)
  log_r=log_r + (G**2-Gtilde2)/2
  u = math.log(np.random.uniform())
  if u<=log_r:
    param_next = param_tilde
  else:
    param_next=param
    reject=True
  if periodic:
    param_next = param_next - (xmax-xmin)*math.floor((param_next-xmin)/(xmax-xmin))
  return param_next,reject




def D_function(param, diffusion, mesh_step, xmin, xmax, I):
    param_ref = param - (xmax-xmin) * math.floor((param - xmin) / (xmax-xmin))
    idx = math.floor((param_ref - xmin)/mesh_step)
    if idx < I - 1:
        ref = xmin + idx*mesh_step
        d = (diffusion[idx + 1] - diffusion[idx]) / mesh_step * (param_ref - ref) + diffusion[idx]
    else:
        ref = xmin + (I-1) * mesh_step
        d = (diffusion[0] - diffusion[I-1]) / mesh_step * (param_ref - ref) + diffusion[I-1]
    return d

    
def D_grad(param, diffusion, mesh_step, xmin, xmax):
    param_ref = param - (xmax-xmin) * math.floor((param - xmin) / (xmax-xmin))
    idx = math.floor((param_ref - xmin)/mesh_step)
    I = diffusion.shape[0]
    if idx<I-1:
        grad_val = (diffusion[idx+1]-diffusion[idx])/mesh_step
    else:
        grad_val = (diffusion[0]-diffusion[I-1])/mesh_step
    return grad_val    


"""
MALA algorithm optimal diffusion
"""

def step_mala_optim(param, time_step, diffusion, mesh_step, potential, grad_potential, beta, xmin, xmax, periodic):
  reject=False
  I = diffusion.shape[0]
  G=np.random.normal(size=1)
  param_tilde = param + time_step*(-D_function(param, diffusion, mesh_step, xmin, xmax, I)*grad_potential(param)+D_grad(param, diffusion, mesh_step, xmin, xmax)/beta)
  param_tilde = param_tilde +math.sqrt(2*time_step*D_function(param, diffusion, mesh_step, xmin, xmax, I)/beta)*G
  log_r = beta*(potential(param)-potential(param_tilde))
  Gtilde2 = (param-param_tilde+time_step*(D_function(param_tilde, diffusion, mesh_step, xmin, xmax, I)*grad_potential(param_tilde)-D_grad(param_tilde, diffusion, mesh_step, xmin, xmax)/beta))**2/(2*time_step*D_function(param_tilde, diffusion, mesh_step, xmin, xmax, I)/beta)
  log_r=log_r + (G**2-Gtilde2)/2 + (math.log(D_function(param, diffusion, mesh_step, xmin, xmax, I)) - math.log(D_function(param_tilde, diffusion, mesh_step, xmin, xmax, I)))/2
  u = math.log(np.random.uniform())
  if u<=log_r:
    param_next = param_tilde
  else:
    param_next=param
    reject=True
  if periodic:
      param_next = param_next - (xmax - xmin) * math.floor((param_next - xmin) / (xmax - xmin))
  return param_next,reject



"""
RWMH
"""


def step_RWMH(param, time_step, potential, beta, xmin, xmax, periodic = True):
    reject = False
    G = np.random.normal(size=1)
    param_tilde = param + math.sqrt(2 * time_step) * G
    log_r = beta*(potential(param) - potential(param_tilde))
    u = math.log(np.random.uniform())
    if u < log_r:
        param_next = param_tilde
    else:
        param_next = param
        reject = True
    if periodic:
        param_next = param_next - (xmax - xmin) * math.floor((param_next - xmin) / (xmax - xmin))
    return param_next, reject


"""
RWMH algorithm optimal diffusion
"""


def step_RWMH_optim(param, time_step, diffusion, mesh_step, potential, beta, xmin, xmax, periodic = True):
    reject = False
    I = diffusion.shape[0]
    G = np.random.normal(size=1)
    param_tilde = param + math.sqrt(2 * time_step * D_function(param, diffusion, mesh_step, xmin, xmax, I)) * G
    log_r = beta*(potential(param) - potential(param_tilde))
    Gtilde2 = (param - param_tilde) ** 2 / (2 * time_step * D_function(param_tilde, diffusion, mesh_step, xmin, xmax,I))
    log_r = log_r + (G ** 2 - Gtilde2) / 2 + (
                math.log(D_function(param, diffusion, mesh_step, xmin, xmax, I)) - math.log(D_function(param_tilde, diffusion, mesh_step, xmin, xmax, I))) / 2
    u = math.log(np.random.uniform())
    if u <= log_r:
        param_next = param_tilde
    else:
        param_next = param
        reject = True
    if periodic:
        param_next = param_next - (xmax - xmin) * math.floor((param_next - xmin) / (xmax - xmin))
    return param_next, reject
