#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 10:08:24 2020

@author: genevieverobin
"""

import numpy as np
import math
import optim_1D

"""
Numerical parameters
"""

n_values = 2*np.array([1, 2, 3, 4, 5])
K = n_values.shape[0]
constr = 'l2'

xmin = 0
xmax = 1
tol = 1e-6
beta = 1

for j in range(K):
    # Parameters
    n = n_values[j]
    I = 200 * n
    mesh = np.linspace(xmin, xmax, I + 1)
    dx = (xmax - xmin) / I

    """
    Potential
    """
    #n=4
    # def V(param):
    #     return math.cos(2 * math.pi * n * param)

    # def V(param):
    #     return math.sin(2 * math.pi * n * param) * (2 + math.sin(math.pi * n * param))

    def V(param):
        return math.sin(2 * n * math.pi * param) * (2 + math.sin(n * math.pi * param))


    # Piecewise linear constant potential
    pi = np.zeros(I)
    for i in range(I):
        pi[i] = math.exp(-beta * V((mesh[i] + mesh[i + 1]) / 2))
    pi = pi / (sum(pi) * dx)


    res = optim_1D.optim_algo(pi, dx, I, 3, None, 1)
    D = res['x']
    lambdaD = -res['fun']

    D_homog = np.zeros(I)
    for i in range(I):
        D_homog[i] = math.exp((2 * beta * V(mesh[i]) / 3))
    D_homog = D_homog / math.sqrt(np.dot(D_homog, pi * D_homog) * dx)

    np.savetxt("results/Dopt_homogeneisation_n{}_constr{}.txt".format(n, constr), D)
    np.savetxt("results/Dhomog_homogeneisation_n{}_constr{}.txt".format(n, constr), D_homog)
    np.savetxt("results/mesh_homogeneisation_n{}_constr{}.txt".format(n, constr), mesh)
    np.savetxt("results/pi_homogeneisation_n{}_constr{}.txt".format(n, constr), pi)
    np.savetxt("results/lambda_homogeneisation_n{}_constr{}.txt".format(n, constr), [lambdaD])
