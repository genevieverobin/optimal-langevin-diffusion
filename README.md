# Optimal Langevin diffusion

This repository contains Python code to optimize the diffusion of overdamped Langevin dynamics. The method is described in the article "Optimizing the diffusion of overdamped Langevin dynamics" by T. Lelièvre, G. Pavliotis, G. Robin and G. Stoltz.

## Method

Overdamped Langevin dynamics are used in many MCMC sampling algorithms to produce new states. An important parameter of such dynamics is the diffusion function—the diffusion can be somewhat interpreted as a step size—which has fundamental impact on the convergence rate of Langevin dynamics. The method developed in this repository aims to compute the optimal diffusion function to maximize the convergence speed of Langevin dynamics towards the target measure. The resulting optimal diffusion can be plugged into MCMC algorithms (RWMH, MALA, etc.) to accelerate their convergence towards equilibrium. The code available here concerns only *one-dimensional* problems.

## Files

The repository contains code implementing the method:
- optim_1D.py contains the optimization algorithm.
- algos_sampling.py contains RWMH and MALA algorithms where the optimal diffusion computed with optim_1D can be used as parameter.

The repository contains examples and experiments:
- potentials.py contains examples of one-dimensional potentials (minus log of target measure).
- experiments_optim.py contains experiments to visualize the optimal diffusion for simple potentials
- experiments_sampling.py contains experiments to visualize the effect of the optimal diffusion in MCMC sampling algorithms
- experiments_homogeneisation.py contains experiments to visualize the behaviour of the optimal diffusion in the asymptotic regime of periodic homogenization, for simple potentials

## Support
Contact genevieve.robin@cnrs.fr

## Authors and acknowledgment
Developer and maintainer: Geneviève Robin.

## License
For open source projects, say how it is licensed.


