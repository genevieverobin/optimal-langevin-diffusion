#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 11:12 2021
@author: genevieverobin
"""
import math
import numpy as np
import matplotlib.pyplot as plt
import optim_1D
from importlib import reload
import pandas as pd
reload(optim_1D)


def V(param):
    return math.sin(2 * math.pi * param) * (2 + math.sin(math.pi * param))

def gradV(param):
    grad_val = 2 * math.pi * math.cos(2 * math.pi * param) * (2 + math.sin(math.pi * param))
    grad_val = grad_val + math.pi * math.cos(math.pi * param) * math.sin(2 * math.pi * param)
    return grad_val

constr = 'l2'
xmin = -1
xmax = 1
beta = 1
# Parameters
#I = 500
I = 1000
mesh = np.linspace(xmin, xmax, I + 1)
dx = (xmax - xmin) / I


pi = np.zeros(I)
for i in range(I):
    pi[i] = math.exp(-beta * V((mesh[i] + mesh[i + 1]) / 2))
pi = pi / (sum(pi) * dx)
plt.plot(mesh[range(I)], pi)

res = optim_1D.optim_algo(pi, dx, I, 3, None, 1, tolerance=1e-18, maxiter = 10000, disp = True, method = 'SLSQP')
D_optim = res['x']
#np.savetxt("results/D_optim.txt", D_optim)
plt.plot(mesh[range(I)], D_optim)


D_homog = np.zeros(I)
for i in range(I):
    D_homog[i] = math.exp((2 * beta * V(mesh[i]) / 3))
D_homog = D_homog /  math.sqrt(np.dot(D_homog, pi * D_homog) * dx)
#np.savetxt("results/D_homog.txt", D_homog)
plt.plot(mesh[range(I)], D_homog)

print(res.fun)
M = np.zeros((I, I))
for i in range(I - 1):
    M[i, i] = M[i, i] + pi[i] * dx / 3
    M[i + 1, i + 1] = M[i + 1, i + 1] + pi[i] * dx / 3
    M[i, i + 1] = M[i, i + 1] + pi[i] * dx / 6
    M[i + 1, i] = M[i + 1, i] + pi[i] * dx / 6
M[I - 1, I - 1] = M[I - 1, I - 1] + pi[I - 1] * dx / 3
M[0, 0] = M[0, 0] + pi[I - 1] * dx / 3
M[I - 1, 0] = M[I - 1, 0] + pi[I - 1] * dx / 3
M[0, I - 1] = M[0, I - 1] + pi[I - 1] * dx / 3

print(optim_1D.lambda_value(D_homog,pi, dx, I, M, k=3)[0])
print(optim_1D.lambda_value(D_optim,pi, dx, I, M, k=3)[0])
print(optim_1D.lambda_value(np.repeat(1,I),pi, dx, I, M, k=3)[0])


## Raffinage du maillage
I = I*2
D_optim = np.repeat(D_optim, 2)
mesh = np.linspace(xmin, xmax, I + 1)
dx = (xmax - xmin) / I

pi = np.zeros(I)
for i in range(I):
    pi[i] = math.exp(-beta * V((mesh[i] + mesh[i + 1]) / 2))
pi = pi / (sum(pi) * dx)
plt.plot(mesh[range(I)], pi)

res = optim_1D.optim_algo(pi, dx, I, 3, D_optim, 1, tolerance=1e-18, maxiter = 10000, disp = True, method = 'SLSQP')
D_optim = res['x']
np.savetxt("results/D_optim.txt", D_optim)

plt.plot(mesh[range(I)], D_optim)
plt.plot(mesh[range(I)], pi)
plt.show()

val_lambda, phiD = optim_1D.lambda_value(D_optim, pi, dx, I, M, k=3)
plt.plot(mesh[range(200,500)], D_optim[range(200,500)])
plt.plot(mesh[range(200,500)], phiD[range(200,500)])
plt.show()

def phi_grad(param, phi, mesh_step, xmin):
    idx = math.floor((param - xmin)/mesh_step)
    I = phi.shape[0]
    if idx<I-1:
        grad_val = (phi[idx+1]-phi[idx])/mesh_step
    else:
        grad_val = (phi[0]-phi[I-1])/mesh_step
    return grad_val

grad_phiD = map(lambda x: phi_grad(x, phiD, dx, xmin), mesh)
grad_phiD = list(grad_phiD)
grad_phiD = np.array(grad_phiD)

plt.plot(mesh[range(I)], D_optim[range(I)])
plt.plot(mesh[range(I)], phiD[range(I)])
plt.plot(mesh[range(I)], np.max(D_optim)*grad_phiD[range(I)]**2/np.max(grad_phiD[range(I)]**2), 'dashed')
plt.show()

df_D = pd.DataFrame({'mesh': mesh[range(I)], 'pi':pi[range(I)], 'phiD':phiD[range(I)], 'grad_phiD': np.max(D_optim)*grad_phiD[range(I)]**2/np.max(grad_phiD[range(I)]**2), 'D_optim': D_optim[range(I)], 'phiD': phiD[range(I)]})
plt.plot('mesh', 'D_optim', data=df_D, marker='s', markerfacecolor='red', markevery = 50, markersize=0, color='red',
         linewidth=2, label="optimal diffusion")
plt.plot('mesh', 'grad_phiD', data=df_D, marker='D', markerfacecolor='green', markevery = 50, markersize=0, color='black',
         linewidth=2, linestyle=':', label="|grad_phiD|^2")
plt.plot('mesh', 'phiD', data=df_D, marker='D', markerfacecolor='green', markevery = 50, markersize=0, color='black',
         linewidth=1, linestyle='-', label="phiD")
plt.plot('mesh', 'pi', data=df_D, marker='D', markerfacecolor='green', markevery = 50, markersize=0, color='red',
         linewidth=1, linestyle='-', label="pi")
plt.plot('mesh', 'D_homog', data=df_D, marker='D', markerfacecolor='green', markevery=50, markersize=0, color='red',
         linewidth=1, linestyle='-', label="pi")
plt.legend()



df_D = pd.DataFrame({'mesh': mesh[range(I)], 'pi':pi[range(I)], 'V':map(V, mesh[range(I)])})
plt.plot('mesh', 'pi', data=df_D, marker='s', markerfacecolor='red', markevery = 50, markersize=0, color='red',
         linewidth=2, label="target distribution")
plt.plot('mesh', 'V', data=df_D, marker='D', markerfacecolor='green', markevery = 50, markersize=0, color='black',
         linewidth=2, linestyle=':', label="potential")
plt.legend()
